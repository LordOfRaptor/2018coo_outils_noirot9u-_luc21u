package TD2;

public class Loup implements Animal{
	
	private int viande;
	private int pointDeVie;
	
	
	public Loup() {
		this.pointDeVie=30;
		this.viande=0;
	}
	
	public Loup(int ptsVie) {
		if(ptsVie<0) {
			this.pointDeVie=0;
		}else {
			this.pointDeVie=ptsVie;
		}
	}
	
	@Override
	public boolean etreMort() {
		if(this.pointDeVie==0) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public boolean passerUnjour() {
		boolean res = false;
		if(!this.etreMort()) {
			this.viande= (int) this.viande/2;
			if(this.viande-1<0) {
				if(this.pointDeVie<4) {
					this.pointDeVie=0;
				}else {
					this.pointDeVie-=4;	
				}
			}else {
				this.viande--;
			}
		}else {
			res=false;
		}
		if(!this.etreMort()) {
			res=true;
		}
		return res;
	}

	@Override
	public void stockerNourriture(int nourriture) {
		if(nourriture>0) {
			this.viande+=nourriture;
		}
	}

	@Override
	public int getPv() {
		return this.pointDeVie;
	}

	@Override
	public int getStockNourriture() {
		return this.viande;
	}
	
	public String toString() {
		return ("Loup - pv:"+this.pointDeVie+" viande:"+this.viande);
	}

}
