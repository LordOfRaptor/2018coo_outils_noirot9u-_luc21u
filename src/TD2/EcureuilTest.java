package TD2;

import static org.junit.Assert.*;

import org.junit.Test;

public class EcureuilTest {

	@Test
	public void testEcureuil() {
		Ecureuil e = new Ecureuil();
		assertEquals("l ecureuil devrait avoir 5 points de vie ",5,e.getPv());
		assertEquals("l ecureuil devrait avoir 0 noisette ",0,e.getStockNourriture());
	}

	@Test
	public void testEcureuilInf() {
		Ecureuil e = new Ecureuil(-5);
		assertEquals("l ecureuil devrait avoir 0 points de vie ",0,e.getPv());
		assertEquals("l ecureuil devrait avoir 0 noisette ",0,e.getStockNourriture());
	}
	
	public void testEcureuilSup() {
		Ecureuil e = new Ecureuil(50);
		assertEquals("l ecureuil devrait avoir 0 points de vie ",50,e.getPv());
		assertEquals("l ecureuil devrait avoir 0 noisette ",0,e.getStockNourriture());
	}
	
	public void testEcureuilNull() {
		Ecureuil e = new Ecureuil(0);
		assertEquals("l ecureuil devrait avoir 0 points de vie ",0,e.getPv());
		assertEquals("l ecureuil devrait avoir 0 noisette ",0,e.getStockNourriture());
	}

	@Test
	public void testEtreMort() {
		Ecureuil e = new Ecureuil(0);
		assertEquals("l'ecureuil devrait etre mort",true,e.etreMort());
	}

	@Test
	public void testPasserUnjourFalse() {
		Ecureuil e = new Ecureuil(2);
		e.passerUnjour();
		assertEquals("l'ecureuil devrait etre mort",true,e.etreMort());
		assertEquals("l'ecureuil ne devrait pas passer le jour",false,e.passerUnjour());
	}
	
	@Test
	public void testPasserUnjourTrue() {
		Ecureuil e = new Ecureuil(2);
		e.stockerNourriture(5);
		assertEquals("l'ecureuil ne devrai pas etre mort",false,e.etreMort());
		assertEquals("l'ecureuil devrait passer le jour",true,e.passerUnjour());
		assertEquals("l ecureuil devrait avoir 3 noisette ",3,e.getStockNourriture());
	}

	@Test
	public void testStockerNourriture() {
		Ecureuil e = new Ecureuil();
		e.stockerNourriture(5);
		assertEquals("l ecureuil devrait avoir 5 noisette ",5,e.getStockNourriture());
	}
	
	@Test
	public void testStockerNourritureInf() {
		Ecureuil e = new Ecureuil();
		e.stockerNourriture(-5);
		assertEquals("l ecureuil devrait avoir 0 noisette ",0,e.getStockNourriture());
	}
	
	@Test
	public void testStockerNourritureNull() {
		Ecureuil e = new Ecureuil();
		e.stockerNourriture(0);
		assertEquals("l ecureuil devrait avoir 0 noisette ",0,e.getStockNourriture());
	}


}
