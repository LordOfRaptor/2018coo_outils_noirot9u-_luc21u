package TD2;

public class Ecureuil implements Animal {

    private int noisette;
    private int pointDeVie;

    public Ecureuil() {
        this.pointDeVie = 5;
        this.noisette = 0;
    }

    public Ecureuil(int pv) {
        if (pv >= 0) {
            this.pointDeVie = pv;
        }
        else{
            this.pointDeVie = 0;
        }
        this.noisette = 0;
    }

    @Override
    public boolean etreMort() {
        if(getPv() == 0){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public boolean passerUnjour() {
        if(!this.etreMort()) {
            if (getStockNourriture() > 0) {
                this.noisette--;
                if (getStockNourriture() > 0) {
                    this.noisette--;
                }
                return true;
            } else {
                if(this.getPv() >=2){
                    this.pointDeVie -=2;
                    return true;
                }
                else {
                    this.pointDeVie = 0;
                    return false;
                }
            }
        }
        else{
            return false;
        }
    }

    @Override
    public void stockerNourriture(int nourriture) {
        if (nourriture >=0) {
            this.noisette += nourriture;
        }

    }

    @Override
    public int getPv() {
        return this.pointDeVie;
    }

    @Override
    public int getStockNourriture() {
        return this.noisette;
    }

    @Override
    public String toString() {
        return ("Ecureuil - pv: "+this.getPv()+ " noisettes:" + this.getStockNourriture());
    }
}
