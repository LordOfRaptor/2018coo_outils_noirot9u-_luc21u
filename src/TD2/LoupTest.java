package TD2;

import org.junit.Test;

import static org.junit.Assert.*;

public class LoupTest {

    @Test
    public void Test_Loup(){
        Loup l = new Loup();
        assertEquals("Devrait avoir 30 pv",30,l.getPv());
        assertEquals("Devrait avoir 0 viande",0,l.getStockNourriture());
    }

    @Test
    public void Test_Loup_Param1(){
        Loup l = new Loup(-15);
        assertEquals("Devrait avoir 0 pv",0,l.getPv());
        assertEquals("Devrait avoir 0 viande",0,l.getStockNourriture());
    }
    @Test
    public void Test_Loup_Param2(){
        Loup l = new Loup(0);
        assertEquals("Devrait avoir 0 pv",0,l.getPv());
        assertEquals("Devrait avoir 0 viande",0,l.getStockNourriture());
    }

    @Test
    public void Test_Loup_Param3(){
        Loup l = new Loup(33);
        assertEquals("Devrait avoir 33 pv",33,l.getPv());
        assertEquals("Devrait avoir 0 viande",0,l.getStockNourriture());
    }


    @Test
    public void Test_passerUnjour1() {
        Loup l = new Loup(0);
        l.stockerNourriture(5);
        assertEquals("Ne devrait pas passer le jour",false,l.passerUnjour());
    }

    @Test
    public void Test_passerUnjour2() {
        Loup l = new Loup(30);
        l.stockerNourriture(5);
        assertEquals("devrait passer le jour",true,l.passerUnjour());
        assertEquals("Devrait avoir 1 viande",1,l.getStockNourriture());
    }

    @Test
    public void Test_passerUnjour3() {
    }

    @Test
    public void Test_stockerNourriture1() {
        Loup l = new Loup();
        l.stockerNourriture(5);
        assertEquals("Devrait avoir 5 viande",5,l.getStockNourriture());
    }

    @Test
    public void Test_stockerNourriture2() {
        Loup l = new Loup();
        l.stockerNourriture(-5);
        assertEquals("Devrait avoir 0 viande",0,l.getStockNourriture());
    }

    @Test
    public void Test_stockerNourriture3() {
        Loup l = new Loup();
        l.stockerNourriture(-5);
        assertEquals("Devrait avoir 0 viande",0,l.getStockNourriture());
    }

}